package lead.consult;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;


public class WebDriverService {
    @Test
    public void testContactUsForm() {

        //Web Driver
        WebDriver driver = new ChromeDriver();
        driver.get(ElementConstants.BASE_URL);
        driver.manage().window().maximize();

        //Open Contact Us page
        WebElement contactUSButton = driver.findElement(By.id(ElementConstants.CONTACT_US_BUTTON_ID));
        contactUSButton.click();

        //Click to 'Your name' input field and write something
        WebElement YOUR_NAME = driver.findElement(By.xpath(ElementConstants.YOUR_NAME_INPUT_FIELD));
        YOUR_NAME.sendKeys(ElementConstants.YOUR_NAME_TEXT);

        // Click to 'Your Email' input field and write something
        WebElement YOUR_EMAIL = driver.findElement(By.xpath(ElementConstants.YOUR_EMAIL_INPUT_FIELD));
        YOUR_EMAIL.sendKeys(ElementConstants.YOUR_EMAIL_TEXT);

        // Click to 'Your phone number' input field and write something
        WebElement YOUR_PHONE_NUMBER = driver.findElement(By.xpath(ElementConstants.YOUR_PHONE_NUMBER_INPUT_FIELD));
        YOUR_PHONE_NUMBER.sendKeys(ElementConstants.YOUR_PHONE_NUMBER_TEXT);

        //Click to 'Your message' input field and write something
        WebElement YOUR_MESSAGE = driver.findElement(By.xpath(ElementConstants.YOUR_MESSAGE_INPUT_FIELD));
        YOUR_MESSAGE.sendKeys(ElementConstants.YOUR_MESSAGE_TEXT);

        //Iframe switch
        WebElement IFRAME = driver.findElement(By.xpath(ElementConstants.IFRAME_CHANGE));
        driver.switchTo().frame(IFRAME);

        //Click to the checkbox to confirm
        WebElement CHECKBOX = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By.xpath(ElementConstants.IM_NOT_A_ROBOT_CHECKBOX)));
        CHECKBOX.click();

        // Switch back to the main frame
        driver.switchTo().defaultContent();

        // Checkbox Terms&Condition
        WebElement TERMS_AND_CONDITIONS = driver.findElement(By.xpath(ElementConstants.TERMS_AND_CONDITIONS));
        TERMS_AND_CONDITIONS.click();

        //Send button
        WebElement SEND_BUTTON_CLICK = driver.findElement(By.xpath(ElementConstants.SEND_BUTTON));
        SEND_BUTTON_CLICK.click();

        // Assert that the success message is displayed
        WebElement successMessage = new WebDriverWait(driver, Duration.ofSeconds(25)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ElementConstants.SUCCESS_MASSAGE)));
        Assert.assertTrue(successMessage.isDisplayed(), "Thank you for your message. It has been sent.");

        driver.quit();


    }

    @Test
    public void test2ContactUsForm()
    {
        //Web Driver
        WebDriver driver = new ChromeDriver();
        driver.get(ElementConstants.BASE_URL);
        driver.manage().window().maximize();

        //Open Contact Us page
        WebElement contactUSButton = driver.findElement(By.id(ElementConstants.CONTACT_US_BUTTON_ID));
        contactUSButton.click();

        //Click to 'Your name' input field and write something
        WebElement YOUR_NAME = driver.findElement(By.xpath(ElementConstants.YOUR_NAME_INPUT_FIELD));
        YOUR_NAME.sendKeys(ElementConstants.YOUR_NAME_TEXT);

        // Click to 'Your phone number' input field and write something
        WebElement YOUR_PHONE_NUMBER = driver.findElement(By.xpath(ElementConstants.YOUR_PHONE_NUMBER_INPUT_FIELD));
        YOUR_PHONE_NUMBER.sendKeys(ElementConstants.YOUR_PHONE_NUMBER_TEXT);

        //Click to 'Your message' input field and write something
        WebElement YOUR_MESSAGE = driver.findElement(By.xpath(ElementConstants.YOUR_MESSAGE_INPUT_FIELD));
        YOUR_MESSAGE.sendKeys(ElementConstants.YOUR_MESSAGE_TEXT);

        //Iframe switch
        WebElement IFRAME = driver.findElement(By.xpath(ElementConstants.IFRAME_CHANGE));
        driver.switchTo().frame(IFRAME);

        //Click to the checkbox to confirm
        WebElement CHECKBOX = new WebDriverWait(driver,Duration.ofSeconds(10) ).until(ExpectedConditions.elementToBeClickable(By.xpath(ElementConstants.IM_NOT_A_ROBOT_CHECKBOX)));
        CHECKBOX.click();

        // Switch back to the main frame
        driver.switchTo().defaultContent();

        // Checkbox Terms&Condition
        WebElement TERMS_AND_CONDITIONS = driver.findElement(By.xpath(ElementConstants.TERMS_AND_CONDITIONS));
        TERMS_AND_CONDITIONS.click();

        //Send button
        WebElement SEND_BUTTON_CLICK = driver.findElement(By.xpath(ElementConstants.SEND_BUTTON));
        SEND_BUTTON_CLICK.click();

        // Assert that the message is displayed
        WebElement successMessage = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ElementConstants.VALIDATION_TEXT_MISSING_FIELDS)));
        Assert.assertTrue(successMessage.isDisplayed(), "One or more fields have an error. Please check and try again.");

        driver.quit();

    }

}

