package lead.consult;


public final class ElementConstants
{
    public static final String BASE_URL = "https://www.leadconsult.eu/";
    public static final String CONTACT_US_BUTTON_ID = "menu-item-5819";
    public static final String YOUR_NAME_INPUT_FIELD = "(//input[@class='wpcf7-form-control wpcf7-text wpcf7-validates-as-required'])[1]";
    public static final String YOUR_NAME_TEXT = "Peicho Binbelov";
    public static final String YOUR_EMAIL_INPUT_FIELD = "//input[@class='wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email']";
    public static final String YOUR_EMAIL_TEXT = "binbelovpeicho@gmail.com";
    public static final String YOUR_PHONE_NUMBER_INPUT_FIELD = "//input[@class='wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel']";
    public static final String YOUR_PHONE_NUMBER_TEXT = "0879947799";
    public static final String YOUR_MESSAGE_INPUT_FIELD = "//textarea[@class='wpcf7-form-control wpcf7-textarea']";
    public static final String YOUR_MESSAGE_TEXT = "This is a test !";
    public static final String IM_NOT_A_ROBOT_CHECKBOX = "//div[@class='recaptcha-checkbox-border']";
    public static final String IFRAME_CHANGE = "//iframe[@title='reCAPTCHA']";
    public static final String TERMS_AND_CONDITIONS = "(//input[@type='checkbox'])[1]";
    public static final String SEND_BUTTON = "//input[@class='wpcf7-form-control has-spinner wpcf7-submit' and @value='Send']";
    public static final String SUCCESS_MASSAGE = "(//div[@class='wpcf7-response-output'])[1]";
    //public static final String YOUR_NAME_VALIDATION_TEXT = "(//span[@class='wpcf7-not-valid-tip'])[1]";
    //public static final String YOUR_EMAIL_VALIDATION_TEXT = "(//span[@class='wpcf7-not-valid-tip'])[2]";
    //public static final String IM_NOT_A_ROBOT_VALIDATION_CHECKBOX = "(//span[@class='wpcf7-not-valid-tip'])[3]";
    //public static final String IM_NOT_A_ROBOT_VALIDATION_VALIDATION_TEXT = "(//span[@class='wpcf7-not-valid-tip'])[4]";
    public static final String VALIDATION_TEXT_MISSING_FIELDS = "(//div[@class='wpcf7-response-output'])[1]";



}
