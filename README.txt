Web Driver Project with Selenium and Java
This is a project for automating web browser interactions using Selenium and Java, with TestNG as the testing framework.

Setup
To get started with this project, follow these steps:

Clone the repository to your local machine.
Ensure that you have Java and Maven installed on your machine.
Run mvn clean install in the project directory to install the required dependencies.